//
//  BankTests.swift
//  BankTests
//
//  Created by Karolina on 10/02/2019.
//  Copyright © 2019 Hennis. All rights reserved.
//

import XCTest
@testable import Bank


class BankTests: XCTestCase {
    var newPlayer: Player!
    var storyboard: UIStoryboard!
    var newVC: ViewController!
    var view : UIView!
    
    
    override func setUp() {
        newPlayer = Player()
        storyboard = UIStoryboard(name: "Main", bundle: nil)
        newVC = (storyboard.instantiateInitialViewController() as! ViewController)
        view = newVC.view
    }

    override func tearDown() {
        newPlayer = nil
        newVC = nil
        view = nil
        storyboard = nil
    }
    
    func testInitialization() {
        XCTAssertEqual(newPlayer.money, 1500)
        XCTAssertEqual(newPlayer.name, "")
    }
    
    func testTable(){
        XCTAssertEqual(ViewController.playerList.count, 0)
        newVC.name!.text = "Karolina"
        newVC.addPlayer(self)
        XCTAssertEqual(ViewController.playerList.count, 1)
    }
    
    func testAddEmptyName(){
        newVC.name!.text = "   "
        newVC.addPlayer(self)
        XCTAssertEqual(ViewController.playerList.count, 0)
    }
    
    
}
