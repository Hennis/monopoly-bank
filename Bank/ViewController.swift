//
//  ViewController.swift
//  Bank
//
//  Created by Karolina on 10/02/2019.
//  Copyright © 2019 Hennis. All rights reserved.
//

import UIKit

class Player{
    var name = ""
    var money = 1500
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ViewController.playerList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = ViewController.playerList[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    static var playerList: [Player] = []
    @IBOutlet var name: UITextField!
    @IBOutlet var firstTable: UITableView!
    
    
    @IBAction func addPlayer(_ sender: Any) {
        if !(name.text!.trimmingCharacters(in: .whitespaces).isEmpty){
        let newPlayer = Player()
        newPlayer.name = name.text!
        ViewController.playerList.append(newPlayer)
        firstTable.reloadData()
        }
        name.placeholder = "nazwa gracza"
        name.text = ""
        self.view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

