//
//  ViewController2.swift
//  Bank
//
//  Created by Karolina on 10/02/2019.
//  Copyright © 2019 Hennis. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
}

class ViewController2: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var playerTable: UITableView!
    @IBOutlet weak var historyTable: UITableView!
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if tableView == self.playerTable{
            count = ViewController.playerList.count}
        if tableView == self.historyTable{
            count = payHistory.count
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell?
        if tableView == self.playerTable{
            let ownCell = tableView.dequeueReusableCell(withIdentifier: "test", for: indexPath) as! TableViewCell
        
        let index = ViewController.playerList[indexPath.row]
        ownCell.label1?.text = index.name
        ownCell.label2?.text = String(index.money)
        cell = ownCell
        }
        if tableView == self.historyTable{
        cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
            cell!.textLabel?.textAlignment = .center
            cell!.textLabel?.text = payHistory[indexPath.row]
        }
        return cell!
    }
    
    var rowIndex = -1
    var payHistory = [String]()
    var historyRecord = ""
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        rowIndex = indexPath.row
        self.view.endEditing(true)
    }
    @IBOutlet weak var payValue: UITextField!
    @IBOutlet weak var addValue: UITextField!
    
    
    @IBAction func pay(_ sender: Any) {
        if payValue.text != ""{
        ViewController.playerList[rowIndex].money = ViewController.playerList[rowIndex].money - Int(payValue.text!)!
        historyRecord = (ViewController.playerList[rowIndex].name + "    -" + payValue.text!)
        payHistory.insert(historyRecord, at: 0)
        playerTable.reloadData()
        historyTable.reloadData()
        rowIndex = -1
        payValue.text = ""
        self.view.endEditing(true)
        }
    }
    
    @IBAction func add(_ sender: Any) {
        if addValue.text != ""{
        ViewController.playerList[rowIndex].money = ViewController.playerList[rowIndex].money + Int(addValue.text!)!
        historyRecord = (ViewController.playerList[rowIndex].name + "    +" + addValue.text!)
        payHistory.insert(historyRecord, at: 0)
        playerTable.reloadData()
        historyTable.reloadData()
        rowIndex = -1
        addValue.text = ""
            self.view.endEditing(true)}
    }
    @IBOutlet weak var alert: UIView!
    
    @IBAction func restart(_ sender: Any) {
      alert.isHidden = false
    }
    
    @IBAction func restartConfirm(_ sender: Any) {
        ViewController.playerList.removeAll()
        alert.isHidden = true
    }
    
    @IBAction func alertCancel(_ sender: Any) {
        alert.isHidden = true
    }
}


